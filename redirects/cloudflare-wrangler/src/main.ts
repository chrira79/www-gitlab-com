import { Redirects } from './redirects';
import redirectsYaml from '../../../data/redirects.yml';
import yaml from 'js-yaml';

declare const DEBUG: boolean;

export interface Env {
  REDIRECTS: KVNamespace;
  ENVIRONMENT: string;
  BUCKET: string;
  DEFAULT_CACHE_TTL: number;
}

const STORAGE_BASE_URL = `https://storage.googleapis.com`;
const DEFAULT_CACHE_TTL = 60;

export default {
  async fetch(clientRequest: Request, env: Env, ctx: ExecutionContext): Promise<Response> {
    const clientRequestURL = new URL(clientRequest.url);
    const { pathname, search, hostname } = clientRequestURL;

    // Redirect/strip leading "www."
    if (hostname.startsWith('www.')) {
      return Response.redirect(`https://${hostname.slice(4)}${pathname}${search}`, 301);
    }

    // If URL ends with `/index.html` then redirect to the same path minus the `/index.html`
    if (pathname.endsWith('/index.html')) {
      const pathWithoutIndexHtml = pathname.replace(/\/index.html$/, '/');
      return Response.redirect(`https://${hostname}${pathWithoutIndexHtml}${search}`, 301);
    }

    // Redirect requests from China with a path that starts
    // with /install to about.gitlab.cn
    if (clientRequest.cf?.country === 'CN' && pathname.startsWith('/install')) {
      return Response.redirect(`https://about.gitlab.cn${pathname}${search}`, 301);
    }

    let originUrl = `${STORAGE_BASE_URL}/${env.BUCKET}`;
    let redirects;

    if (env.ENVIRONMENT === 'review') {
      // Prefix path with subdomain for review app
      const subdomain = hostname.split('.', 1)[0];
      originUrl += `/${subdomain}`;

      // Check if we have a redirect key for this MR
      if (env.REDIRECTS) {
        const mrRedirects = await env.REDIRECTS.get(subdomain);

        if (mrRedirects !== null) {
          if (DEBUG) {
            console.debug(`Using redirects from KV key '${subdomain}' for ${clientRequest.url}...`);
          }

          redirects = new Redirects(yaml.load(mrRedirects));
        }
      }
    }

    // Save originUrl before appending stuff to it
    const baseUrl = originUrl;

    if (!redirects) {
      if (DEBUG) {
        console.debug(`Using redirects from the worker for ${clientRequest.url}...`);
      }
      redirects = new Redirects(yaml.load(redirectsYaml));
    }

    let redirect = redirects.getExactMatch(pathname, `https://${hostname}`);
    if (redirect) {
      if (DEBUG) {
        console.debug(`FOUND - exact match redirect for '${pathname}' => redirecting to: ${redirect}`);
      }

      return Response.redirect(redirect, 301);
    }

    redirect = redirects.getStartsWithMatch(pathname, hostname, search, `https://${hostname}`);
    if (redirect) {
      if (DEBUG) {
        console.debug(`FOUND - 'starts with' regex match for '${pathname}' => redirecting to: ${redirect}`);
      }
      return Response.redirect(redirect, 301);
    }

    redirect = redirects.getAppendMatch(pathname, hostname, search, `https://${hostname}`);
    if (redirect) {
      if (DEBUG) {
        console.debug(`FOUND - 'append' regex match for '${pathname}' => redirecting to: ${redirect}`);
      }
      return Response.redirect(redirect, 301);
    }

    // Add trailing slash if needed (if there is no query string)
    if (search === '' && /\/[^./]+$/.test(pathname)) {
      if (DEBUG) {
        console.debug(`Missing trailing slash => redirecting to: ${clientRequest.url}/`);
      }
      return Response.redirect(`${clientRequest.url}/`, 301);
    }

    let path = pathname;
    if (path.endsWith('/')) {
      // eg: / => /index.html
      // eg: /get-started/ => /get-started/index.html
      path += 'index.html';
    } else if (/^(.*)\/([^.]+)$/.test(path)) {
      // eg: /search => /search/index.html
      path += '/index.html';
    }

    // ... append the rest of the URL
    originUrl += `${path}${search}`;

    // Fetch from the origin
    const beRequest = new Request(originUrl, clientRequest);
    const beResponse = await fetch(beRequest, {
      cf: {
        cacheKey: `https://${hostname}${pathname}${search}`,
        cacheEverything: true,
        cacheTtlByStatus: {
          '200-299': env.DEFAULT_CACHE_TTL || DEFAULT_CACHE_TTL,
          404: 1,
          '500-599': 0,
        },
      },
    });

    if (beResponse.status === 404) {
      const notFoundPage = `${baseUrl}/404.html`;

      if (DEBUG) {
        console.debug(`Got a 404 for ${originUrl}. Returning ${notFoundPage}`);
      }

      return fetch(notFoundPage);
    }

    // Reconstruct the Response object to make its headers mutable
    const clientResponse = new Response(beResponse.body, beResponse);

    // CORS control
    if (/^https?:\/\/about\.gitlab\.com$/.test(clientRequestURL.origin)) {
      clientResponse.headers.set('Access-Control-Allow-Origin', clientRequestURL.origin);
    }

    clientResponse.headers.append('Vary', 'Origin');

    return clientResponse;
  },
};
