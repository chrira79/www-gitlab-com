---
layout: security
title: Security - Hardening Your GitLab Instance
description: "We designed this Hardening page to serve as a starting point for those interested in hardening a GitLab instance to help improve security."
canonical_path: "/security/hardening/"
---

{::options parse_block_html="true" /}

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# GitLab Instance Hardening
<br>
Every deployment of a GitLab instance is going to differ. So the various security settings you have to adjust will vary greatly depending on your use case, your risk assessment, and your environment. How do you know which choices are the right ones? One particular feature might be secure and completely fine in one environment, whereas in another environment there might be some risk.

How does one get started? GitLab has you covered! Below are several resources that can help you get started. If you decide to go *really* deep, we can handle that as well.

----

### Starting Resources

We have a lot of [information on security](https://docs.gitlab.com/ee/security/) in a GitLab environment. While this is a lot of information, here are a few quick tips for helping you to secure your GitLab instance quickly.

- Account access:
  - It is possible to disable new sign-ups, require administrative approval for new sign-ups, and a few other features. Refer to the documentation on [Sign-up restrictions](https://docs.gitlab.com/ee/user/admin_area/settings/sign_up_restrictions.html) for more information.
  - There are numerous settings for further locking down user accounts, including Admin Mode, multi-factor authentication, and more. Refer to the documentation on [Sign-in restrictions](https://docs.gitlab.com/ee/user/admin_area/settings/sign_in_restrictions.html) for more information.

- Protecting data:
  - There are numerous settings for [controlling access](https://docs.gitlab.com/ee/user/admin_area/settings/visibility_and_access_controls.html) you can set.
  - Adjustments can be made to specifically [adjust and control SSH access](https://docs.gitlab.com/ee/security/ssh_keys_restrictions.html) to your data.
  - There are protections you can enforce on [CI secrets](https://docs.gitlab.com/search/?q=vault) and pipelines in [protected environment](https://docs.gitlab.com/ee/ci/environments/protected_environments.html) to help secure your CI/CD process.

Read through and follow the advice in these blog posts. They cover more of the above information in detail, and in many cases this will meet all of your needs.

- [GitLab instance: security best practices](https://about.gitlab.com/blog/2020/05/20/gitlab-instance-security-best-practices/)

- [How to harden your self-managed GitLab instance](https://about.gitlab.com/blog/2023/05/23/how-to-harden-your-self-managed-gitlab-instance/)


### Going Deeper

If you've followed the quick recommendations above and still want some additional help (even down to the operating system level), we've included in our documentation some in-depth security recommendations. While tailored for the self-managed instance, most of the settings apply to all deployments of GitLab.

- [GitLab Hardening Recommendations](https://docs.gitlab.com/ee/security/hardening.html)

### Additional Resources

Be sure to check the [Security FAQ](/security/faq/index.md) for information on how we do security here at GitLab, as well as the best ways to keep in touch with the latest and greatest involving security and GitLab.

