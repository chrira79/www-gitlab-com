---
layout: handbook-page-toc
title: "GitLab Assembly"
description: "At the start of every Fiscal Year (beginning of February), the E-Group hosts a GitLab Assembly meeting"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## GitLab Assembly

GitLab Assembly takes place each quarter and is part of a series of events intended to share timely information with our team members. Assembly happens after reporting our earnings to share highlights of the past quarter and celebrate our successes, milestones, and results.

Timing of team member touchpoints including Assembly:

- [Quarterly Kickoff](https://about.gitlab.com/handbook/ceo/chief-of-staff-team/#quarterly-kickoff) (first half of the first month of the quarter): Typically a video update from GitLab CEO, Sid Sibrandij shared on Slack, followed by an AMA
- Earnings Call (first half of the second month of the quarter): Public call that team members are invited to attend
- Earnings Recap & AMA (immediately following earnings): A 25min private AMA occurring after Earnings
- Assembly (end of second month, early the third month of a quarter): An all-company meeting occurring 2-3 weeks after Earnings that covers a wide range of topics

### GitLab Assembly Schedule

Quarterly: Three standalone GitLab Assemblies per year, one GitLab Assembly-type content incorporated into [GitLab Contribute](https://about.gitlab.com/events/gitlab-contribute/){:data-ga-name="Gitlab contribute"}{:data-ga-location="body"}. For example, in FY22, the GitLab Assembly schedule was:

1. Q1 Assembly: [Fiscal year kickoff](https://about.gitlab.com/company/gitlab-assembly/#fy22-gitlab-assembly){:data-ga-name="Fiscal year kickoff"}{:data-ga-location="body"}
1. Q2 Assembly: Recap Q1 and look forward to rest of Q2
1. Q3 Assembly: Recap Q2 and look forward to rest of Q3
1. Q4 Assembly: Recap Q3 and look forward to year-end/Q4

### GitLab Assembly Format

Assembly is a quarterly, company-wide 40 minute meeting consisting of ~25 mins content and ~15 mins Q&A. It is scheduled at two separate times so team members in all time zones have the opportunity to attend.

The call concludes with an AMA with all of E-Group. Team members can ask questions of any particular executive. The AMA doc will be circulated early so that team members can submit questions.

[Team members should join muted](/handbook/tools-and-tips/zoom/setting-up-a-zoom-meeting#large-meeting-considerations), but they can unmute themselves as required to effectively participate in the conversation. 

### GitLab Assembly Content

GitLab Assembly content creation is led by the [People Comms & Engagement](https://handbook.gitlab.com/job-families/people-group/people-communications-engagement/){:data-ga-name="internal comms"}{:data-ga-location="body"} team and [E-Group](https://about.gitlab.com/company/team/structure/#executives){:data-ga-name="e-group"}{:data-ga-location="body"}.

This is a [People Comms & Engagement](https://handbook.gitlab.com/job-families/people-group/people-communications-engagement/){:data-ga-name="internal comms initiative"}{:data-ga-location="body"} initiative and @kaylagolden is the DRI. The meeting is scheduled by the [EBA to the CEO](https://handbook.gitlab.com/job-families/people-group/executive-business-administrator/){:data-ga-name="EBA"}{:data-ga-location="body"}. The call is recorded and posted to Google Drive. The recording is internal only because we share sensitive information confidential to the company at the meeting.

### GitLab Assembly Timeline for delivery

Starting in FY23-Q4, the following timeline will be used to prepare key deliverables that will be needed to hold the event in close proximity to our [earnings call](/handbook/finance/investor-relations/#quarterly-earnings-process). For a more detailed list of items needed for delivery, please visit the [People Communications & Engagement](/handbook/people-group/employment-branding/people-communications/#gitlab-assembly) handbook page.

1. 4 weeks after the prior quarters earnings call: set time/date for Assembly (note this isn't sent out to all team members until after the earnings call date has been announced)
1. 4 weeks prior to Assembly: have host & speakers identified
1. 4 weeks prior to Assembly: Assembly CEO content draft prepared, five review sessions scheduled with Sid spread over the next two weeks
1. 3 weeks prior to Assembly: Begin feedback sessions with Sid and continuously iterate on the feedback
1. 2 weeks prior to Assembly: Meeting invites sent to all team members, content "pencils down"
1. 2 weeks prior to Assembly: Kickoff Legal review of CEO content
1. 1 week prior to Assembly: CEO content finalized, and video recorded with Sid
1. 1 week prior to Assembly: Legal review of video after recording
1. 1 week prior to Assembly: Work with the digital production team to have the slides and video edited together. Get Legal review of this as well.

### GitLab Assembly Goals

1. 📈 Review values, mission, vision, strategy
1. 🤝 Review of the past quarter and celebration of our success, milestones, and results
1. 👁 Transparent AMA with a majority of the company in attendance
1. 🌐 Network with global team members

### GitLab Assembly Metrics

Attendance:

1. FY22-Q4: >65% global attendance
1. FY23-Q1: >70% global attendance
1. FY23-Q2: >60% global attendance
1. FY23-Q3: >60% global attendance

### FY22 GitLab Assembly

The FY22 GitLab Assembly (previously called Fiscal Year Kickoff) took place 2021-02-18 at 8:00am and 5:00pm Pacific Time. It was attended live by approximately 800 team members, with remaining team members encouraged to watch the recording asynchronously.

The event was internal because it featured financial metrics and other information which was not public. It was recorded, and the recording is available [in Drive](https://drive.google.com/file/d/1V_yohghvDpKQf4sXlNGe_6LgscVjxfXo/view?usp=sharing).

This event was approximately 40 minutes long and was hosted on Hopin. The agenda was as follows:

1. Welcome
1. FY21/22 Highlights - [CEO](https://handbook.gitlab.com/job-families/chief-executive-officer/){:data-ga-name="CEO"}{:data-ga-location="body"} & [CRO](https://handbook.gitlab.com/job-families/sales/chief-revenue-officer/){:data-ga-name="CRO"}{:data-ga-location="body"}
1. GitLab is in Rarefied Air - [CPO](https://handbook.gitlab.com/job-families/people-group/chief-people-officer/){:data-ga-name="CPO"}{:data-ga-location="body"} & [CFO](https://handbook.gitlab.com/job-families/finance/chief-financial-officer/){:data-ga-name="CFO"}{:data-ga-location="body"}
1. Marketing Vision - [CMO](https://handbook.gitlab.com/job-families/marketing/chief-marketing-officer/){:data-ga-name="CMO"}{:data-ga-location="body"}
1. Product Themes - [Chief Product Officer](https://handbook.gitlab.com/job-families/product/chief-product-officer/){:data-ga-name="chief product officer"}{:data-ga-location="body"}
1. Product Vision - [VP Product Management](https://handbook.gitlab.com/job-families/product/product-management-leadership/#vp-of-product-management){:data-ga-name="VP product management"}{:data-ga-location="body"}
1. Engineering Vision - [CTO](https://handbook.gitlab.com/job-families/engineering/engineering-management/#chief-technology-officer){:data-ga-name="CTO"}{:data-ga-location="body"}
1. FY22 Vision - CEO & CRO
1. Closing and randomized coffee chats

Based on [feedback from the FY21 kickoff](#improvements-to-be-made){:data-ga-name="FY21 feedback"}{:data-ga-location="body"} and in the spirit of iteration, there were some changes to the format. Changes included:

* Adding a produced video, created mostly using highlights from [Sales Kickoff](/handbook/sales/training/SKO/){:data-ga-name="SKO"}{:data-ga-location="body"} presentations, to share the most important information. This mostly replaced the previous format of sequential 5-minute live updates.
* Adding a second time to be inclusive of all time zones.
* During each event, attendees watched the video update live (no requirement to complete any actions before the meeting). The video is followed by an AMA with the executive team.
* More focus on the big picture, inspiration, and team alignment.
* Inclusivity: the video was captioned and industry terms, acronyms, or jargon which aren’t well-known were defined.

[Read the feedback](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/issues/4196){:data-ga-name="Read the feedback"}{:data-ga-location="body"} from GitLab Assembly FY22 Kickoff

### FY21 Kickoff

The FY21 Kickoff call was the first of its sort.
The recording is on [GitLab Unfiltered](https://youtu.be/XlYsmj5fCcI) (GitLab Internal).
The recording is internal because we talk about financial metrics which are not public.
The slides can be found in the Drive under the title "2020-02-06 FY21 Kickoff".

We collected feedback in [cos-team#15](https://gitlab.com/gitlab-com/cos-team/issues/15){:data-ga-name="cos-team"}{:data-ga-location="body"}.

The structure was roughly as follows:

* 5 minutes: Intro, Welcome, Overview
* 5 minutes: CEO Overview- Accomplishments, Values, Upcoming, Key Focus
* 5 minutes: Sales
* 5 minutes: Product
* 5 minutes: Product Strategy
* 5 minutes: Engineering
* 5 minutes: People
* 5 minutes: Marketing (Standup break)
* 5 minutes: Finance
* 5 minutes: Legal
* Remaining time: AMA with all execs

Feedback on the event was overall positive, with over 600 team members attending.

![fy21_kickoff_attendance_stats](fy21_kickoff_attendance_stats.png)

#### Improvements to be made

There were a number of problems that presented themselves. Below is a list of the problems and some proposed solutions to be solved for the FY22 Kickoff

| Problem | Possible Solution |
|---------|-------------------|
| People were locked out of the doc. |* We have 1-5 people designated as "typers" and everyone else gets view-only access of the doc. Questions are submitted via the Zoom chat.<br>* Could use a slack channel and threaded responses.<br> * GitLab Issues|
| People were locked out of the slides. | Share a PDF of the slides in #companyannouncements before the call, so that people who are locked out can still follow along. |
| Because this was a last minute idea that we executed on, we were only able to coordinate for one time. | We should have this at least twice, once in EMEA friendly time zones and one in APAC friendly time zones. Earlier scheduling will make this possible. |
| There were a lot of acronyms and concepts where it would be helpful to have a primer ahead of time so I could track better — things like ARR or CSM come to mind. | Make sure all acronyms are defined upfront. |
| The slide presenter dance | The "next slide please" dance? Something else? |
| Kickoff felt a bit like an extended GC and not visionary enough | Tailor content more appropriately to inspirational/visionary, especially at the beginning |
