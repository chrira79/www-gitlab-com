require 'nokogiri'
require 'yaml'

module ReleasePosts
  class Deprecations
    include Helpers

    def initialize
      @deprecations_url = "https://docs.gitlab.com/ee/update/deprecations.html"
      @deprecations_dir = File.join(git_repo_path, 'data', 'release_posts')
      @deprecations_file = File.join(@deprecations_dir, 'deprecations-index.yml')
      @deprecations = {}
    end

    def generate
      prepare
      find_deprecations
      save_deprecations
      commit_deprecations
    end

    private

    def prepare
      puts "Fetching branches"
      git_fetch

      puts 'Set username and email'
      git_config('user.email', 'job+bot@gitlab.com')
      git_config('user.name', 'Bot')

      puts "Switching branch to #{next_release}"
      git_change_branch(next_release)
    end

    def page
      Nokogiri::HTML(URI.open(@deprecations_url))
    end

    def find_deprecations
      milestones = page.css('.milestone-wrapper')

      # Iterate over each group
      milestones.each do |milestone|
        # Get the release number
        release = milestone.css('h2').first.content.tr('Announced in: GitLab ', '').tr("\n", '')

        # Initialize an array for the release if it doesn't exist
        @deprecations[release] ||= []

        # Select the deprecation titles
        h3s = milestone.css('h3')

        # Add the titles to the array
        h3s.each do |h3|
          @deprecations[release] << h3.content.tr("\n", '')
        end
      end
    end

    def save_deprecations
      # Write the deprecations to the YAML file
      File.write(@deprecations_file, @deprecations.to_yaml)
    end

    def commit_deprecations
      git_add(@deprecations_file)
      git_commit("Updated deprecations content")
      git_push("https://jobbot:#{ENV.fetch('GITLAB_BOT_TOKEN', nil)}@gitlab.com/gitlab-com/www-gitlab-com.git", next_release)
    end

    def fetch_release_branches
      (git_branch_list('origin/release-*') || '').split
    end

    def next_release
      release_branch = fetch_release_branches
        .select { |name| name =~ %r{\Aorigin/release-\d+-\d+\Z} }
        .map { |name| name.delete_prefix('origin/') }
        .max_by { |name| sort_by_version(name) }

      unless release_branch
        puts "Release branch is missing!"
        exit 1
      end

      puts "Detected release: #{release_branch}"
      release_branch
    end

    def sort_by_version(release)
      version = release.match(/(\d+)-(\d+)/)
      [version[1].to_i, version[2].to_i]
    end
  end
end
